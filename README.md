# docker-nginx-certbot

dockerize a setup for initializing ssl certs on new deployments

### Getting started

This makes the most sense to clone straight into the project root. The letsencrypt directory will mount via the docker-compose file, so your certs will be preserved for exporting, and the appropriate file in conf.d will also be updated (if you choose) by certbot.

Modify the server_name as required in conf.d/default.conf, i.e. I changed mine to nginx.garyburgmann.com

```bash
$ git clone https://gitlab.com/garyburgmann/docker-nginx-certbot
$ cat conf.d/default.conf
server {
    listen 80;
    listen [::]:80;

    root /var/www/example.com/html;
    index index.html index.htm index.nginx-debian.html;

    server_name nginx.garyburgmann.com;

    location / {
        try_files $uri $uri/ =404;
    }
}
$ docker-compose up -d
Creating network "docker-nginx-certbot_default" with the default driver
Creating docker-nginx-certbot_nginx-cerbot_1 ... done
$ docker exec -it docker-nginx-certbot_nginx-cerbot_1 bash
```

### Running certbot
Once inside the container, just run some simple certbox nginx commands.

```bash
$ certbot --nginx -d nginx.garyburgmann.com
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator nginx, Installer nginx
Enter email address (used for urgent renewal and security notices) (Enter 'c' to
cancel): garyburgmann@gmail.com

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
agree in order to register with the ACME server at
https://acme-v02.api.letsencrypt.org/directory
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(A)gree/(C)ancel: a

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about our work
encrypting the web, EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: n
Obtaining a new certificate
Performing the following challenges:
http-01 challenge for nginx.garyburgmann.com
2020/01/12 11:59:07 [notice] 18#18: signal process started
Waiting for verification...
Cleaning up challenges
2020/01/12 11:59:11 [notice] 20#20: signal process started
Deploying Certificate to VirtualHost /etc/nginx/conf.d/default.conf
2020/01/12 11:59:14 [notice] 22#22: signal process started

Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: No redirect - Make no further changes to the webserver configuration.
2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
new sites, or if you're confident your site works on HTTPS. You can undo this
change by editing your web server's configuration.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 2
Redirecting all traffic on port 80 to ssl in /etc/nginx/conf.d/default.conf
2020/01/12 12:03:23 [notice] 24#24: signal process started

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Congratulations! You have successfully enabled https://nginx.garyburgmann.com

You should test your configuration at:
https://www.ssllabs.com/ssltest/analyze.html?d=nginx.garyburgmann.com
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/nginx.garyburgmann.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/nginx.garyburgmann.com/privkey.pem
   Your cert will expire on 2020-04-11. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot again
   with the "certonly" option. To non-interactively renew *all* of
   your certificates, run "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

### Did it work?

Check the certs are mounted back on the host (and back them up!)
```bash
$ exit
$ ls letsencrypt/live/
README  nginx.garyburgmann.com
$ ls letsencrypt/live/nginx.garyburgmann.com/
README  cert.pem  chain.pem  fullchain.pem  privkey.pem
```

### Check out config quality

Certbot will automatically update your config to redirect all traffic to https (if you allow). This gave me a score of "A": https://www.ssllabs.com/ssltest/analyze.html?d=nginx.garyburgmann.com

You can view the mounted config back on the host.

```bash
$ cat conf.d/default.conf
server {

    root /var/www/example.com/html;
    index index.html index.htm index.nginx-debian.html;

    server_name nginx.garyburgmann.com;

    location / {
        try_files $uri $uri/ =404;
    }

    listen [::]:443 ssl ipv6only=on; # managed by Certbot
    listen 443 ssl; # managed by Certbot
    ssl_certificate /etc/letsencrypt/live/nginx.garyburgmann.com/fullchain.pem; # managed by Certbot
    ssl_certificate_key /etc/letsencrypt/live/nginx.garyburgmann.com/privkey.pem; # managed by Certbot
    include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
    ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot

}

server {
    if ($host = nginx.garyburgmann.com) {
        return 301 https://$host$request_uri;
    } # managed by Certbot


    listen 80;
    listen [::]:80;

    server_name nginx.garyburgmann.com;
    return 404; # managed by Certbot
```

### Renewal

If this container is left running, python-certbot-nginx is kind enough to handle checking twice daily for upcoming expiry of these certs, and running renewal process as necessary. A dry run can be performed to ensure there will be no hiccups when this process runs for real in the future.

```bash
$ docker exec -it docker-nginx-certbot_nginx-cerbot_1 certbot renew --dry-run
Saving debug log to /var/log/letsencrypt/letsencrypt.log

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Processing /etc/letsencrypt/renewal/nginx.garyburgmann.com.conf
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Cert not due for renewal, but simulating renewal for dry run
Plugins selected: Authenticator nginx, Installer nginx
Renewing an existing certificate
Performing the following challenges:
http-01 challenge for nginx.garyburgmann.com
2020/01/12 12:19:15 [notice] 44#44: signal process started
Waiting for verification...
Cleaning up challenges
2020/01/12 12:19:19 [notice] 46#46: signal process started
2020/01/12 12:19:22 [notice] 48#48: signal process started

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
new certificate deployed with reload of nginx server; fullchain is
/etc/letsencrypt/live/nginx.garyburgmann.com/fullchain.pem
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
** DRY RUN: simulating 'certbot renew' close to cert expiry
**          (The test certificates below have not been saved.)

Congratulations, all renewals succeeded. The following certs have been renewed:
  /etc/letsencrypt/live/example.garyburgmann.com/fullchain.pem (success)
  /etc/letsencrypt/live/nginx.garyburgmann.com/fullchain.pem (success)
** DRY RUN: simulating 'certbot renew' close to cert expiry
**          (The test certificates above have not been saved.)
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

IMPORTANT NOTES:
 - Your account credentials have been saved in your Certbot
   configuration directory at /etc/letsencrypt. You should make a
   secure backup of this folder now. This configuration directory will
   also contain certificates and private keys obtained by Certbot so
   making regular backups of this folder is ideal.
```